import re, os, copy
import random
import pandas as pd
import shutil
import seaborn as sns
import matplotlib.pyplot as plt



def doBoxPlot(fileName,xAxis,yAxis,hueCol,graphDf):
    if(hueCol == None):
        ax2 = sns.boxplot(y=yAxis, x=xAxis, data=graphDf)
    else:
        ax2 = sns.boxplot(y=yAxis, x=xAxis, hue = hueCol,data=graphDf)
    fig = ax2.get_figure()
    fig.savefig(fileName, format='svg', dpi=1500)
    ax2.clear()


def doSwarmPlot(fileName,xAxis,yAxis,graphDf):
    ax2 = sns.swarmplot(y=yAxis, x=xAxis, data=graphDf)
    fig = ax2.get_figure()
    fig.savefig(fileName, format='svg', dpi=1500)
    ax2.clear()


listOfSCkt = ["c432","c499","c1908","c3540"]


seriesName = "iscas85"
rootData = "/home/user1/currentResearch/atpg_structural/code"
tempDataPath = rootData + os.sep + "tempData"
testDataRootFolder = rootData + os.sep + "testData" + os.sep + "tessent"
dumpDataRootFolder = rootData + os.sep + "dumpData" + os.sep + "tessent" + os.sep + seriesName
csvDelimiter = ","
verilogExtension = ".v"
testFileExtension = ".spf"
atpgLogFilePathExtension = ".atpg.log"
atpgResultFilePathExtension = ".atpg.result"

doFileExtension = ".dofile"
cellLibraryPath = testDataRootFolder + os.sep + "cellLibrary.atpg"
numCircuitToBeGenerated = 40

# Choice (Random or with Dual Gates)
choice = 1

thresholdPercent = {0.1: "tenPercent",
                    0.3: "thirtyPercent",
                    0.5: "fiftyPercent",
                    0.7: "seventyPercent",
                    0.9: "ninetyPercent"}


for inputCkt in listOfSCkt:
    inputCktNameSynth = inputCkt + "_synth"
    dumpDataFolder = dumpDataRootFolder + os.sep + inputCktNameSynth
    resultLogFilePath = dumpDataFolder + os.sep + "results_" + inputCktNameSynth + ".csv"

    redundancyBinInformationCombinedFilePath = dumpDataFolder + os.sep + "redundancyBinResultsCombined_" + inputCktNameSynth + ".csv"
    redundancyBinInformationIndividualFilePath = dumpDataFolder + os.sep + "redundancyBinResultsIndividual_" + inputCktNameSynth + ".csv"

    rIndividualLogFileHandler = open(redundancyBinInformationIndividualFilePath,'w+')
    rIndividualLogFileHandler.write("cktName" + csvDelimiter + "redundancyBin" + csvDelimiter + "percentChange" + csvDelimiter + "TESTABLE_FAULTS" + csvDelimiter + "OVERALL_FAULTS" + csvDelimiter + "ATPG_COV_TESTABLE" + csvDelimiter + "ATPG_COV_OVERALL" + csvDelimiter + "SIBLING_TEST_PATTERN_COV_TESTABLE"+ csvDelimiter + "SIBLING_TEST_PATTERN_COV_OVERALL")
    rIndividualLogFileHandler.write("\n")

    rCombinedLogFileHandler = open(redundancyBinInformationCombinedFilePath, 'w+')
    rCombinedLogFileHandler.write("cktName" + csvDelimiter + "redundancyBin" + csvDelimiter + "covPercentage" + csvDelimiter + "covType")
    rCombinedLogFileHandler.write("\n")



    atpgDataFrame = pd.read_csv(resultLogFilePath)
    maxRedundantFault = atpgDataFrame['UT'][atpgDataFrame['UT'].idxmax()]
    atpgDataFrame['utLabel'] = pd.cut(atpgDataFrame['UT'], 5)
    groupedData = atpgDataFrame.groupby(['utLabel'])
    sameRedundancyBucketCkts = {}
    for gKey in groupedData.groups.keys():
        sameRedundancyBucketCkts[gKey] = []
        listOfIndex = groupedData.groups[gKey]
        for idx in listOfIndex:
            sameRedundancyBucketCkts[gKey].append(atpgDataFrame.iloc[idx])

    countBin = 0
    for redunKey in sorted(sameRedundancyBucketCkts.keys()):
        for rowEntry in sameRedundancyBucketCkts[redunKey]:
            redundantFolderPath = dumpDataFolder + os.sep + "bin_" + str(0)
            for simulateOtherRow in sameRedundancyBucketCkts[redunKey]:
                baseName = rowEntry['cktName'] + "_" + simulateOtherRow['cktName']
                simulateResultFile = redundantFolderPath + os.sep + "simulateLogFile_" + baseName + atpgResultFilePathExtension

                if(not os.path.exists(simulateResultFile)):
                    for x in range(1,5):
                        redundantFolderPath = dumpDataFolder + os.sep + "bin_" + str(x)
                        simulateResultFile = redundantFolderPath + os.sep + "simulateLogFile_" + baseName + atpgResultFilePathExtension
                        if(os.path.exists(simulateResultFile)):
                            break

                readSimulationLogFileHandler = open(simulateResultFile,'r+')
                simulateResultFileLines = readSimulationLogFileHandler.readlines()
                numSimulatedFaultCountLine = simulateResultFileLines[7]
                percentCovUsingSiblingATPGLine = simulateResultFileLines[-1]

                numSimulateFaultLineWords = re.findall('\w+',numSimulatedFaultCountLine)
                totSimulatedFaultCount = int(numSimulateFaultLineWords[-1])
                percentCovUsingSiblingATPGLineWords = re.findall('\w+',percentCovUsingSiblingATPGLine)
                percentCovUsingSiblingATPG = float(percentCovUsingSiblingATPGLineWords[1])

                numFaultCoveredBySiblingATPG = int(percentCovUsingSiblingATPG*totSimulatedFaultCount)
                testableFaultCoverage = numFaultCoveredBySiblingATPG/rowEntry["TE"]
                overallFaultCoverage = numFaultCoveredBySiblingATPG/(rowEntry["TE"]+rowEntry["UT"])
                testableFaultCoverage = float("{0:.2f}".format(testableFaultCoverage))
                overallFaultCoverage = float("{0:.2f}".format(overallFaultCoverage))

                rIndividualLogFileHandler.write(baseName+csvDelimiter+str(redunKey).replace(",","-")+csvDelimiter+str(rowEntry["percentChange"])+csvDelimiter+str(rowEntry["TE"])+csvDelimiter+str(rowEntry["TE"]+rowEntry["UT"])+csvDelimiter+str(rowEntry["test_cov"])+csvDelimiter+str(rowEntry["fault_cov"])+csvDelimiter+str(testableFaultCoverage)+csvDelimiter+str(overallFaultCoverage))
                rIndividualLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(rowEntry["test_cov"]) + csvDelimiter + "atpgCovTest")
                rCombinedLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(rowEntry["fault_cov"]) + csvDelimiter + "atpgCovOverall")
                rCombinedLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(testableFaultCoverage) + csvDelimiter + "siblingCovTest")
                rCombinedLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(overallFaultCoverage) + csvDelimiter + "siblingCovOverall")
                rCombinedLogFileHandler.write("\n")

    rIndividualLogFileHandler.close()
    rCombinedLogFileHandler.close()


    testableCombinedDf = pd.read_csv(redundancyBinInformationCombinedFilePath)
    overallCombinedDf = pd.read_csv(redundancyBinInformationCombinedFilePath)
    individualDf = pd.read_csv(redundancyBinInformationIndividualFilePath)

    graphCombinedTestableFaultCoverage = dumpDataFolder + os.sep + "graphTestableFaultCovCombined_" + inputCktNameSynth + ".svg"
    graphCombinedOverallFaultCoverage = dumpDataFolder + os.sep + "graphOverallFaultCovCombined_" + inputCktNameSynth + ".svg"

    graphIndividualTestableATPGCov = dumpDataFolder + os.sep + "graphTestableFaultCovATPG_" + inputCktNameSynth + ".svg"
    graphIndividualTestableSiblingCov = dumpDataFolder + os.sep + "graphTestableFaultCovSIBLING_" + inputCktNameSynth + ".svg"
    graphIndividualOverallATPGCov = dumpDataFolder + os.sep + "graphOverallFaultCovATPG_" + inputCktNameSynth + ".svg"
    graphIndividualOverallSiblingCov = dumpDataFolder + os.sep + "graphOverallFaultCovSIBLING_" + inputCktNameSynth + ".svg"

    graphScatterPlotPercentChangeRedundancyBin = dumpDataFolder + os.sep + "graphRedundancyBinVsPercentChange_" + inputCktNameSynth + ".svg"

    plt.rcParams["xtick.labelsize"] = 8
    plt.figure(figsize=(8, 5))

    #Individual Graph Dumps

    doBoxPlot(graphIndividualTestableATPGCov,"redundancyBin","ATPG_COV_TESTABLE",None,individualDf)
    doBoxPlot(graphIndividualTestableSiblingCov,"redundancyBin","SIBLING_TEST_PATTERN_COV_TESTABLE",None,individualDf)

    doBoxPlot(graphIndividualOverallATPGCov, "redundancyBin", "ATPG_COV_OVERALL", None, individualDf)
    doBoxPlot(graphIndividualOverallSiblingCov, "redundancyBin", "SIBLING_TEST_PATTERN_COV_OVERALL", None,individualDf)

    #Combined Graph Dumps

    testableCombinedDf = testableCombinedDf.drop(testableCombinedDf[(testableCombinedDf['covType'] == 'siblingCovOverall')|(testableCombinedDf['covType'] == 'atpgCovOverall')].index)
    overallCombinedDf = overallCombinedDf.drop(overallCombinedDf[(overallCombinedDf['covType'] == "atpgCovTest") | (overallCombinedDf['covType'] == "siblingCovTest")].index)


    doBoxPlot(graphCombinedTestableFaultCoverage,"redundancyBin","covPercentage","covType",testableCombinedDf)
    doBoxPlot(graphCombinedOverallFaultCoverage,"redundancyBin","covPercentage","covType",overallCombinedDf)

    doSwarmPlot(graphScatterPlotPercentChangeRedundancyBin,"percentChange","TESTABLE_FAULTS",individualDf)
