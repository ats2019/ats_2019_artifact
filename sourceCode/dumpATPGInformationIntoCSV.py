import re,os,copy
import random
import pandas as pd
import shutil

faultCountDict = {}
coverageDict = {}

def cleanAndInitializeFaultCountDict():
    faultCountDict["FU"] = "0"
    faultCountDict["UO"] = "0"
    faultCountDict["UC"] = "0"
    faultCountDict["DS"] = "0"
    faultCountDict["DI"] = "0"
    faultCountDict["PU"] = "0"
    faultCountDict["PT"] = "0"
    faultCountDict["UU"] = "0"
    faultCountDict["TI"] = "0"
    faultCountDict["BL"] = "0"
    faultCountDict["RE"] = "0"
    faultCountDict["AU"] = "0"
    coverageDict["test_coverage"] = "0"
    coverageDict["fault_coverage"] = "0"
    coverageDict["atpg_effectiveness"] = "0"

setOfFaultClass = {"FU","UO","UC","DS","DI","PU","PT","UU","TI","BL","RE","AU"}
setOfCoverageParams = {"test_coverage","fault_coverage","atpg_effectiveness"}
tessentExec = "tessent"


listOfSCkt = ["b10","b14","b15","b22"]

seriesName = "itc99"
rootData = "/home/user1/currentResearch/atpg_structural/code"
tempDataPath = rootData+os.sep+"tempData"
testDataRootFolder = rootData+os.sep+"testData"+os.sep+"tessent"
dumpDataRootFolder = rootData+os.sep+"dumpData"+os.sep+"tessent"+os.sep+seriesName
csvDelimiter = ","
verilogExtension = ".v"
testFileExtension = ".spf"
atpgLogFilePathExtension = ".atpg.log"
atpgResultFilePathExtension = ".atpg.result"

doFileExtension = ".dofile"
cellLibraryPath = testDataRootFolder+os.sep+"cellLibrary.atpg"
numCircuitToBeGenerated = 40

#Choice (Random or with Dual Gates)
choice = 1

thresholdPercent = {0.1 : "tenPercent" ,
                    0.3 : "thirtyPercent",
                    0.5 : "fiftyPercent",
                    0.7 : "seventyPercent",
                    0.9 : "ninetyPercent"}




for inputCkt in listOfSCkt:
    inputCktNameSynth = inputCkt+"_synth"
    dumpDataFolder = dumpDataRootFolder+os.sep+inputCktNameSynth
    resultLogFilePath = dumpDataFolder+os.sep+"results_"+inputCktNameSynth+".csv"

    resultLogFileHandler = open(resultLogFilePath,'w+')
    resultLogFileHandler.write("cktName" + csvDelimiter + "percentChange" + csvDelimiter + "FU" + csvDelimiter + "TE" + csvDelimiter + "UT" + csvDelimiter + "DS" + csvDelimiter + "DI" + csvDelimiter + "PT" + csvDelimiter + "PU"
                               +csvDelimiter+"AU"+csvDelimiter+"UC"+csvDelimiter+"UO"+csvDelimiter+"UU"+csvDelimiter+"TI"+csvDelimiter+"BL"+csvDelimiter+"RE"+csvDelimiter+"test_cov"+csvDelimiter+"fault_cov"+csvDelimiter+"atpg_eff")

    resultLogFileHandler.write("\n")


    for percentValue in thresholdPercent.keys():
        circuitDumpPath = dumpDataFolder + os.sep + thresholdPercent[percentValue]
        for loopVar in range(numCircuitToBeGenerated):
            inputCktName = inputCktNameSynth + "_" + thresholdPercent[percentValue] + "_" + str(loopVar)
            inputCktFilePath = circuitDumpPath+os.sep+inputCktName+verilogExtension
            inputCktFileTestPath = circuitDumpPath+os.sep+inputCktName+testFileExtension
            inputCktFileATPGLogPath = circuitDumpPath+os.sep+inputCktName+atpgLogFilePathExtension
            inputCktFileATPGResultPath = circuitDumpPath+os.sep+inputCktName+atpgResultFilePathExtension
            inputCktATPGDoFile = circuitDumpPath+os.sep+inputCktName+doFileExtension
            cleanAndInitializeFaultCountDict()
            readATPGStatsReportFile = open(inputCktFileATPGResultPath,'r')
            statsResultFileLines = readATPGStatsReportFile.readlines()
            for lineUnderTest in statsResultFileLines:
                words = re.findall('[a-zA-Z0-9._]+',lineUnderTest)

                if(len(words)>1):
                    if(setOfFaultClass.__contains__(words[0])):
                        if(words[0] == "FU"):
                            faultCountDict[words[0]] = words[2]
                        elif(len(words)==4):
                            #In order to remove entries from faultSubclasses
                            faultCountDict[words[0]] = words[2]
                    elif(setOfCoverageParams.__contains__(words[0])):
                        coverageDict[words[0]] = words[1]


            totalTEFaults = int(faultCountDict["DS"])+int(faultCountDict["DI"])+int(faultCountDict["PU"])+int(faultCountDict["PT"])+int(faultCountDict["AU"])\
                            +int(faultCountDict["UC"])+int(faultCountDict["UO"])
            totalUTFaults = int(faultCountDict["UU"])+int(faultCountDict["BL"])+int(faultCountDict["TI"])+int(faultCountDict["RE"])

            resultLogFileHandler.write(inputCktName + csvDelimiter + thresholdPercent[percentValue] + csvDelimiter + faultCountDict["FU"] + csvDelimiter
                                       + str(totalTEFaults) + csvDelimiter + str(totalUTFaults) + csvDelimiter + faultCountDict["DS"] + csvDelimiter
                                       + faultCountDict["DI"] + csvDelimiter + faultCountDict["PT"] + csvDelimiter + faultCountDict["PU"] + csvDelimiter
                                       + faultCountDict["AU"] + csvDelimiter + faultCountDict["UC"] + csvDelimiter + faultCountDict["UO"] + csvDelimiter
                                       + faultCountDict["UU"] + csvDelimiter + faultCountDict["TI"] + csvDelimiter + faultCountDict["BL"] + csvDelimiter
                                       + faultCountDict["RE"] + csvDelimiter + coverageDict["test_coverage"] + csvDelimiter + coverageDict["fault_coverage"]
                                       + csvDelimiter + coverageDict["atpg_effectiveness"])
            resultLogFileHandler.write("\n")

    resultLogFileHandler.close()


    atpgDataFrame = pd.read_csv(resultLogFilePath)
    maxRedundantFault = atpgDataFrame['UT'][atpgDataFrame['UT'].idxmax()]
    atpgDataFrame['utLabel'] = pd.cut(atpgDataFrame['UT'], 5)
    groupedData = atpgDataFrame.groupby(['utLabel'])
    sameRedundancyBucketCkts = {}
    for gKey in groupedData.groups.keys():
        sameRedundancyBucketCkts[gKey] = []
        listOfIndex = groupedData.groups[gKey]
        for idx in listOfIndex:
            sameRedundancyBucketCkts[gKey].append(atpgDataFrame.iloc[idx])

    countBin = 0
    for redunKey in sameRedundancyBucketCkts.keys():
        redundantFolderPath = dumpDataFolder+os.sep+"bin_"+str(countBin)
        if(not os.path.isdir(redundantFolderPath)):
            os.mkdir(redundantFolderPath)
        countBin+=1
        print(redunKey)
        for rowEntry in sameRedundancyBucketCkts[redunKey]:
            srcFolder = dumpDataFolder+os.sep+rowEntry['percentChange']
            srcCktFile = srcFolder+os.sep+rowEntry['cktName']+verilogExtension
            srcDesignNameList = rowEntry['cktName'].split('_')
            shutil.copy(srcCktFile,redundantFolderPath)
            for simulateOtherRow in sameRedundancyBucketCkts[redunKey]:
                simulateDoFile = redundantFolderPath+os.sep+rowEntry['cktName']+"_"+simulateOtherRow['cktName']+doFileExtension
                simulateResultFile = redundantFolderPath+os.sep+"simulateLogFile_"+rowEntry['cktName']+"_"+simulateOtherRow['cktName']+atpgResultFilePathExtension
                simulateLogFile = redundantFolderPath+os.sep+"simulateLogFile_"+rowEntry['cktName']+"_"+simulateOtherRow['cktName']+atpgLogFilePathExtension
                readVerilogFile = srcCktFile
                readTestFile = dumpDataFolder+os.sep+simulateOtherRow['percentChange'] + os.sep + simulateOtherRow['cktName'] + testFileExtension
                simulateParagraph = "set_context patterns -scan\n" \
                            "read_verilog " + readVerilogFile +" \n" \
                            "read_cell_library "+cellLibraryPath + "\n" \
                            "set_logfile_handling "+simulateLogFile+" -rep\n" \
                            "set_current_design "+ srcDesignNameList[0]+"_C \n" \
                            "set_system_mode analysis\n" \
                            "add_fault -all\n" \
                            "set_fault_mode collapsed\n" \
                            "set_fault_type stuck\n" \
                            "read_patterns "+readTestFile +"\n" \
                            "simulate_patterns > "+simulateResultFile+"\n" \
                            "exit\n"

                doFileWriter = open(simulateDoFile,'w+')
                doFileWriter.write(simulateParagraph)
                doFileWriter.close()

                tessantFaultSimulationRunCmd = "tessent -shell -dofile "+simulateDoFile
                os.system(tessantFaultSimulationRunCmd)
