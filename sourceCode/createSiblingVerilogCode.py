import re,os,copy
import random


listOfSCkt = ["i2c_synth"]

seriesName = "epfl"+os.sep+"random_control"
rootData = rootData = "/home/user1/currentResearch/atpg_structural/code"
tempDataPath = rootData+os.sep+"tempData"
testDataRootFolder = rootData+os.sep+"testData"+os.sep+"tessent"
dumpDataRootFolder = rootData+os.sep+"dumpData"+os.sep+"tessent"+os.sep+seriesName
csvDelimiter = " "
numCircuitToBeGenerated = 40

#Choice (Random or with Dual Gates)
choice = 1



for inputCktName in listOfSCkt:
    cktFileName = testDataRootFolder+os.sep+"synthesizedVerilog"+os.sep+seriesName+os.sep+inputCktName +".v"
    dumpDataFolder = dumpDataRootFolder+os.sep+inputCktName

    if(not os.path.exists(dumpDataFolder)):
        os.mkdir(dumpDataFolder)

    inputCktFileHandler = open(cktFileName,'r+')
    inputFileLines = inputCktFileHandler.readlines()

    setOfGates = ["inv01","buf01","nor02","nand02","or02","and02","xor2","xnor2"]
    setOfReplacableGates = ["nor02","nand02","or02","and02"]
    setOfTargetGates = {}


    thresholdPercent = {0.1 : "tenPercent" ,
                    0.3 : "thirtyPercent",
                    0.5 : "fiftyPercent",
                    0.7 : "seventyPercent",
                    0.9 : "ninetyPercent"}


    countGates = 0
    countReplacableGates = 0
    for line in inputFileLines:
        words = re.findall('\w+',line)
        #print(words)
        if(len(words)> 2):
            if(setOfGates.__contains__(words[0])):
                countGates+=1
                if (setOfReplacableGates.__contains__(words[0])):
                    setOfTargetGates[words[1]] = words[0]
                    countReplacableGates+=1

    inputCktFileHandler.close()
    print(countGates)
    print(countReplacableGates)
    print(setOfTargetGates)


    for percentValue in thresholdPercent.keys():
        numTargetGatesToBeReplace = int(percentValue*countReplacableGates)
        circuitDumpPath = dumpDataFolder+os.sep+thresholdPercent[percentValue]
        if(not os.path.exists(circuitDumpPath)): os.mkdir(circuitDumpPath)
        for loopVar in range(numCircuitToBeGenerated):
            randomCktName = inputCktName+"_"+thresholdPercent[percentValue]+"_"+str(loopVar)
            targetGateSet = copy.deepcopy(setOfTargetGates)
            listOfTargetGateSet = list(targetGateSet.keys())
            for numGateToBeChanged in range(numTargetGatesToBeReplace):
                outputGateIdentifier = random.choice(listOfTargetGateSet)
                if(choice == 1):
                    if (targetGateSet[outputGateIdentifier] == "nor02"):
                        targetGateSet[outputGateIdentifier] = "nand02"
                    elif (targetGateSet[outputGateIdentifier] == "nand02"):
                        targetGateSet[outputGateIdentifier] = "nor02"
                    elif (targetGateSet[outputGateIdentifier] == "and02"):
                        targetGateSet[outputGateIdentifier] = "or02"
                    elif (targetGateSet[outputGateIdentifier] == "or02"):
                        targetGateSet[outputGateIdentifier] = "and02"
                else:
                    outputGateFunctionality = random.choice(setOfReplacableGates)
                    targetGateSet[outputGateIdentifier] = outputGateFunctionality

                listOfTargetGateSet.remove(outputGateIdentifier)

            writeCktFile = circuitDumpPath+os.sep+randomCktName+".v"
            writeCktFileHandler = open(writeCktFile,'w+')
            setOfTargetGateNames = set(targetGateSet.keys())
            for writeLine in inputFileLines:
                words = re.findall('\w+', writeLine)
                if(len(words)>2):
                    if(setOfTargetGateNames.__contains__(words[1])):
                        wordToBeChanged = words[0]
                        # For adding dual logics

                        writeLine = writeLine.replace(wordToBeChanged,targetGateSet[words[1]])
                writeCktFileHandler.write(writeLine)
            writeCktFileHandler.close()
