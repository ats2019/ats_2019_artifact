import re, os, copy
import random

tessentExec = "tessent"

listOfSCkt = ["c7552"]


seriesName = "iscas85"
rootData = "/home/user1/currentResearch/atpg_structural/code"
tempDataPath = rootData + os.sep + "tempData"
testDataRootFolder = rootData + os.sep + "testData" + os.sep + "tessent"
dumpDataRootFolder = rootData + os.sep + "dumpData" + os.sep + "tessent" + os.sep + seriesName
csvDelimiter = " "
verilogExtension = ".v"
testFileExtension = ".spf"
atpgLogFilePathExtension = ".atpg.log"
atpgResultFilePathExtension = ".atpg.result"

doFileExtension = ".dofile"
cellLibraryPath = testDataRootFolder + os.sep + "cellLibrary.atpg"
numCircuitToBeGenerated = 40

# Choice (Random or with Dual Gates)
choice = 1

thresholdPercent = {0.1: "tenPercent",
                    0.3: "thirtyPercent",
                    0.5: "fiftyPercent",
                    0.7: "seventyPercent",
                    0.9: "ninetyPercent"}

for inputCkt in listOfSCkt:
    inputCktNameSynth = inputCkt + "_synth"
    dumpDataFolder = dumpDataRootFolder + os.sep + inputCktNameSynth

    for percentValue in thresholdPercent.keys():
        circuitDumpPath = dumpDataFolder + os.sep + thresholdPercent[percentValue]
        for loopVar in range(numCircuitToBeGenerated):
            inputCktName = inputCktNameSynth + "_" + thresholdPercent[percentValue] + "_" + str(loopVar)
            inputCktFilePath = circuitDumpPath + os.sep + inputCktName + verilogExtension
            inputCktFileTestPath = circuitDumpPath + os.sep + inputCktName + testFileExtension
            inputCktFileATPGLogPath = circuitDumpPath + os.sep + inputCktName + atpgLogFilePathExtension
            inputCktFileATPGResultPath = circuitDumpPath + os.sep + inputCktName + atpgResultFilePathExtension
            inputCktATPGDoFile = circuitDumpPath + os.sep + inputCktName + doFileExtension
            paraGraph = "set_context patterns -scan\n" \
                    "read_verilog " + inputCktFilePath +" \n" \
                    "read_cell_library "+cellLibraryPath + "\n" \
                    "set_logfile_handling "+inputCktFileATPGLogPath+" -rep\n" \
                    "set_current_design "+ inputCkt+"\n" \
                    "set_system_mode analysis\n" \
                    "add_fault -all\n" \
                    "set_fault_mode collapsed\n" \
                    "set_fault_type stuck\n" \
                    "create_patterns\n" \
                    "write_patterns "+inputCktFileTestPath+" -stil -rep\n" \
                    "report_statistics > "+inputCktFileATPGResultPath+"\n" \
                    "exit\n"

            doFileWriter = open(inputCktATPGDoFile,'w+')
            doFileWriter.write(paraGraph)
            doFileWriter.close()

            tessantRunCmd = "tessent -shell -dofile "+inputCktATPGDoFile
            os.system(tessantRunCmd)
