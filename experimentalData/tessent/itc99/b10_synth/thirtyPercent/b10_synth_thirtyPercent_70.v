// Benchmark "b10_C" written by ABC on Sat May 11 19:04:43 2019

module b10_C ( 
    R_BUTTON, G_BUTTON, KEY, START, TEST, RTS, RTR, VOTO0_REG_SCAN_IN,
    V_IN_3_, V_IN_2_, V_IN_1_, V_IN_0_, STATO_REG_3__SCAN_IN,
    STATO_REG_2__SCAN_IN, STATO_REG_1__SCAN_IN, STATO_REG_0__SCAN_IN,
    V_OUT_REG_3__SCAN_IN, V_OUT_REG_2__SCAN_IN, V_OUT_REG_1__SCAN_IN,
    V_OUT_REG_0__SCAN_IN, SIGN_REG_3__SCAN_IN, VOTO1_REG_SCAN_IN,
    CTR_REG_SCAN_IN, VOTO3_REG_SCAN_IN, LAST_R_REG_SCAN_IN,
    CTS_REG_SCAN_IN, VOTO2_REG_SCAN_IN, LAST_G_REG_SCAN_IN,
    U212, U211, U210, U233, U234, U235, U236, U237, U209, U238, U208, U239,
    U240, U207, U241, U242, U243  );
  input  R_BUTTON, G_BUTTON, KEY, START, TEST, RTS, RTR,
    VOTO0_REG_SCAN_IN, V_IN_3_, V_IN_2_, V_IN_1_, V_IN_0_,
    STATO_REG_3__SCAN_IN, STATO_REG_2__SCAN_IN, STATO_REG_1__SCAN_IN,
    STATO_REG_0__SCAN_IN, V_OUT_REG_3__SCAN_IN, V_OUT_REG_2__SCAN_IN,
    V_OUT_REG_1__SCAN_IN, V_OUT_REG_0__SCAN_IN, SIGN_REG_3__SCAN_IN,
    VOTO1_REG_SCAN_IN, CTR_REG_SCAN_IN, VOTO3_REG_SCAN_IN,
    LAST_R_REG_SCAN_IN, CTS_REG_SCAN_IN, VOTO2_REG_SCAN_IN,
    LAST_G_REG_SCAN_IN;
  output U212, U211, U210, U233, U234, U235, U236, U237, U209, U238, U208,
    U239, U240, U207, U241, U242, U243;
  wire n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65,
    n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79,
    n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93,
    n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
    n108, n109, n110, n111, n112, n113, n114, n116, n117, n118, n119, n120,
    n121, n123, n124, n125, n126, n127, n128, n129, n131, n132, n133, n135,
    n136, n138, n139, n141, n142, n144, n145, n146, n147, n149, n150, n151,
    n152, n153, n154, n155, n156, n157, n158, n159, n160, n161, n162, n163,
    n164, n165, n166, n167, n168, n169, n170, n171, n173, n174, n175, n176,
    n177, n178, n179, n181, n182, n183, n184, n185, n186, n187, n188, n189,
    n190, n191, n193, n194, n195, n196, n197, n199, n200, n201, n202, n203,
    n204, n205, n206, n207, n208, n209, n210, n211, n213, n214, n215, n216,
    n217, n218, n219, n220, n221, n222, n224, n225, n227, n228, n229, n230,
    n231, n232, n233, n234, n235, n236, n237, n238;
  and02  g000(.A0(V_IN_2_), .A1(V_IN_3_), .Y(n52));
  and02  g001(.A0(V_IN_0_), .A1(V_IN_1_), .Y(n53));
  nand02 g002(.A0(n53), .A1(n52), .Y(n54));
  nand02 g003(.A0(n54), .A1(STATO_REG_0__SCAN_IN), .Y(n55));
  inv01  g004(.A(STATO_REG_2__SCAN_IN), .Y(n56));
  and02  g005(.A0(STATO_REG_1__SCAN_IN), .A1(n56), .Y(n57));
  inv01  g006(.A(STATO_REG_0__SCAN_IN), .Y(n58));
  and02   g007(.A0(STATO_REG_1__SCAN_IN), .A1(RTR), .Y(n59));
  nor02 g008(.A0(n59), .A1(n58), .Y(n60));
  or02   g009(.A0(n60), .A1(n57), .Y(n61));
  or02  g010(.A0(n61), .A1(STATO_REG_3__SCAN_IN), .Y(n62));
  nand02 g011(.A0(n62), .A1(n55), .Y(n63));
  inv01  g012(.A(STATO_REG_1__SCAN_IN), .Y(n64));
  nor02  g013(.A0(STATO_REG_2__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(n65));
  and02  g014(.A0(n65), .A1(n58), .Y(n66));
  nand02 g015(.A0(n66), .A1(n64), .Y(n67));
  or02  g016(.A0(n58), .A1(STATO_REG_2__SCAN_IN), .Y(n68));
  and02  g017(.A0(n68), .A1(STATO_REG_1__SCAN_IN), .Y(n69));
  nand02 g018(.A0(n69), .A1(RTS), .Y(n70));
  and02  g019(.A0(n70), .A1(n67), .Y(n71));
  nand02 g020(.A0(n65), .A1(START), .Y(n72));
  and02   g021(.A0(n72), .A1(n58), .Y(n73));
  and02  g022(.A0(STATO_REG_0__SCAN_IN), .A1(STATO_REG_1__SCAN_IN), .Y(n74));
  nor02  g023(.A0(n56), .A1(RTR), .Y(n75));
  nand02 g024(.A0(n75), .A1(n74), .Y(n76));
  inv01  g025(.A(STATO_REG_3__SCAN_IN), .Y(n77));
  and02  g026(.A0(STATO_REG_1__SCAN_IN), .A1(n77), .Y(n78));
  nand02  g027(.A0(STATO_REG_2__SCAN_IN), .A1(START), .Y(n79));
  nand02 g028(.A0(n79), .A1(n78), .Y(n80));
  and02  g029(.A0(n80), .A1(n76), .Y(n81));
  and02  g030(.A0(n64), .A1(RTR), .Y(n82));
  and02  g031(.A0(n82), .A1(n68), .Y(n83));
  or02  g032(.A0(STATO_REG_0__SCAN_IN), .A1(n64), .Y(n84));
  inv01  g033(.A(RTS), .Y(n85));
  and02  g034(.A0(STATO_REG_2__SCAN_IN), .A1(n85), .Y(n86));
  and02  g035(.A0(n86), .A1(n84), .Y(n87));
  nor02  g036(.A0(n87), .A1(n83), .Y(n88));
  and02  g037(.A0(n88), .A1(n81), .Y(n89));
  or02  g038(.A0(n89), .A1(n73), .Y(n90));
  and02  g039(.A0(n90), .A1(n71), .Y(n91));
  and02  g040(.A0(n91), .A1(n63), .Y(n92));
  or02   g041(.A0(n92), .A1(STATO_REG_0__SCAN_IN), .Y(n93));
  and02  g042(.A0(n93), .A1(STATO_REG_3__SCAN_IN), .Y(n94));
  nand02 g043(.A0(n91), .A1(n63), .Y(n95));
  inv01  g044(.A(n66), .Y(n96));
  nor02  g045(.A0(n96), .A1(TEST), .Y(n97));
  inv01  g046(.A(VOTO1_REG_SCAN_IN), .Y(n98));
  nor02  g047(.A0(n98), .A1(VOTO0_REG_SCAN_IN), .Y(n99));
  inv01  g048(.A(VOTO2_REG_SCAN_IN), .Y(n100));
  nor02  g049(.A0(n100), .A1(VOTO3_REG_SCAN_IN), .Y(n101));
  or02  g050(.A0(n101), .A1(n99), .Y(n102));
  and02  g051(.A0(n102), .A1(n68), .Y(n103));
  and02   g052(.A0(n103), .A1(n97), .Y(n104));
  and02  g053(.A0(n104), .A1(n64), .Y(n105));
  or02  g054(.A0(n105), .A1(n95), .Y(n106));
  and02   g055(.A0(n106), .A1(n94), .Y(U212));
  nand02  g056(.A0(n102), .A1(STATO_REG_0__SCAN_IN), .Y(n108));
  or02   g057(.A0(n108), .A1(n84), .Y(n109));
  or02   g058(.A0(n109), .A1(n92), .Y(n110));
  and02  g059(.A0(n110), .A1(STATO_REG_2__SCAN_IN), .Y(n111));
  xnor2  g060(.A0(STATO_REG_0__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(n112));
  nand02  g061(.A0(n112), .A1(n64), .Y(n113));
  and02   g062(.A0(n113), .A1(n69), .Y(n114));
  and02   g063(.A0(n114), .A1(n111), .Y(U211));
  and02  g064(.A0(n92), .A1(STATO_REG_1__SCAN_IN), .Y(n116));
  nor02  g065(.A0(n78), .A1(n58), .Y(n117));
  or02  g066(.A0(n117), .A1(n95), .Y(n118));
  or02  g067(.A0(n78), .A1(n58), .Y(n119));
  or02   g068(.A0(n119), .A1(n69), .Y(n120));
  or02   g069(.A0(n120), .A1(n118), .Y(n121));
  and02   g070(.A0(n121), .A1(n116), .Y(U210));
  nand02 g071(.A0(n102), .A1(n77), .Y(n123));
  nor02  g072(.A0(STATO_REG_0__SCAN_IN), .A1(STATO_REG_1__SCAN_IN), .Y(n124));
  or02  g073(.A0(n124), .A1(n123), .Y(n125));
  and02   g074(.A0(n69), .A1(n66), .Y(n126));
  or02   g075(.A0(n126), .A1(n125), .Y(n127));
  and02  g076(.A0(n127), .A1(n95), .Y(n128));
  and02  g077(.A0(n92), .A1(STATO_REG_0__SCAN_IN), .Y(n129));
  and02   g078(.A0(n129), .A1(n128), .Y(U233));
  or02  g079(.A0(n83), .A1(VOTO3_REG_SCAN_IN), .Y(n131));
  inv01  g080(.A(n83), .Y(n132));
  and02  g081(.A0(n132), .A1(V_OUT_REG_3__SCAN_IN), .Y(n133));
  and02   g082(.A0(n133), .A1(n131), .Y(U234));
  and02  g083(.A0(n83), .A1(VOTO2_REG_SCAN_IN), .Y(n135));
  and02  g084(.A0(n132), .A1(V_OUT_REG_2__SCAN_IN), .Y(n136));
  or02   g085(.A0(n136), .A1(n135), .Y(U235));
  and02  g086(.A0(n83), .A1(VOTO1_REG_SCAN_IN), .Y(n138));
  or02  g087(.A0(n132), .A1(V_OUT_REG_1__SCAN_IN), .Y(n139));
  or02   g088(.A0(n139), .A1(n138), .Y(U236));
  or02  g089(.A0(n83), .A1(VOTO0_REG_SCAN_IN), .Y(n141));
  and02  g090(.A0(n132), .A1(V_OUT_REG_0__SCAN_IN), .Y(n142));
  or02   g091(.A0(n142), .A1(n141), .Y(U237));
  and02  g092(.A0(STATO_REG_0__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(n144));
  nor02  g093(.A0(STATO_REG_1__SCAN_IN), .A1(TEST), .Y(n145));
  nand02 g094(.A0(n145), .A1(n66), .Y(n146));
  or02  g095(.A0(n146), .A1(SIGN_REG_3__SCAN_IN), .Y(n147));
  or02   g096(.A0(n147), .A1(n144), .Y(U209));
  and02  g097(.A0(n56), .A1(STATO_REG_3__SCAN_IN), .Y(n149));
  or02  g098(.A0(n149), .A1(n84), .Y(n150));
  or02  g099(.A0(n69), .A1(n77), .Y(n151));
  or02   g100(.A0(n151), .A1(n150), .Y(n152));
  and02  g101(.A0(n152), .A1(V_IN_1_), .Y(n153));
  and02  g102(.A0(n98), .A1(KEY), .Y(n154));
  and02  g103(.A0(n154), .A1(n57), .Y(n155));
  nor02  g104(.A0(n155), .A1(n153), .Y(n156));
  inv01  g105(.A(n150), .Y(n157));
  and02  g106(.A0(n157), .A1(n71), .Y(n158));
  or02  g107(.A0(n56), .A1(START), .Y(n159));
  nand02 g108(.A0(n159), .A1(n84), .Y(n160));
  and02  g109(.A0(n160), .A1(n158), .Y(n161));
  or02  g110(.A0(n66), .A1(START), .Y(n162));
  inv01  g111(.A(LAST_G_REG_SCAN_IN), .Y(n163));
  or02  g112(.A0(n163), .A1(G_BUTTON), .Y(n164));
  nand02 g113(.A0(n164), .A1(n162), .Y(n165));
  inv01  g114(.A(KEY), .Y(n166));
  nand02 g115(.A0(n162), .A1(n166), .Y(n167));
  and02  g116(.A0(n167), .A1(n165), .Y(n168));
  or02  g117(.A0(n168), .A1(n161), .Y(n169));
  nand02  g118(.A0(n169), .A1(n156), .Y(n170));
  and02  g119(.A0(n169), .A1(VOTO1_REG_SCAN_IN), .Y(n171));
  or02   g120(.A0(n171), .A1(n170), .Y(U238));
  xor2   g121(.A0(STATO_REG_1__SCAN_IN), .A1(STATO_REG_2__SCAN_IN), .Y(n173));
  or02  g122(.A0(n64), .A1(STATO_REG_3__SCAN_IN), .Y(n174));
  and02  g123(.A0(STATO_REG_1__SCAN_IN), .A1(n85), .Y(n175));
  or02   g124(.A0(n175), .A1(STATO_REG_0__SCAN_IN), .Y(n176));
  or02   g125(.A0(n176), .A1(n174), .Y(n177));
  or02   g126(.A0(n177), .A1(n173), .Y(n178));
  and02  g127(.A0(n178), .A1(CTR_REG_SCAN_IN), .Y(n179));
  and02   g128(.A0(n179), .A1(n87), .Y(U208));
  or02  g129(.A0(n152), .A1(V_IN_3_), .Y(n181));
  xnor2  g130(.A0(VOTO2_REG_SCAN_IN), .A1(VOTO0_REG_SCAN_IN), .Y(n182));
  xor2   g131(.A0(n182), .A1(n98), .Y(n183));
  or02  g132(.A0(n183), .A1(n74), .Y(n184));
  nor02  g133(.A0(n184), .A1(n181), .Y(n185));
  and02  g134(.A0(START), .A1(n166), .Y(n186));
  or02   g135(.A0(n186), .A1(n74), .Y(n187));
  nand02 g136(.A0(n187), .A1(n65), .Y(n188));
  or02  g137(.A0(n188), .A1(n161), .Y(n189));
  nor02  g138(.A0(n189), .A1(n185), .Y(n190));
  and02  g139(.A0(n189), .A1(VOTO3_REG_SCAN_IN), .Y(n191));
  or02   g140(.A0(n191), .A1(n190), .Y(U239));
  and02  g141(.A0(STATO_REG_1__SCAN_IN), .A1(KEY), .Y(n193));
  or02  g142(.A0(n193), .A1(n162), .Y(n194));
  and02  g143(.A0(n194), .A1(R_BUTTON), .Y(n195));
  inv01  g144(.A(LAST_R_REG_SCAN_IN), .Y(n196));
  nor02  g145(.A0(n194), .A1(n196), .Y(n197));
  and02   g146(.A0(n197), .A1(n195), .Y(U240));
  and02  g147(.A0(n58), .A1(STATO_REG_1__SCAN_IN), .Y(n199));
  or02   g148(.A0(n199), .A1(n173), .Y(n200));
  or02   g149(.A0(STATO_REG_0__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(n201));
  and02  g150(.A0(n201), .A1(RTR), .Y(n202));
  and02  g151(.A0(n84), .A1(STATO_REG_3__SCAN_IN), .Y(n203));
  or02   g152(.A0(n203), .A1(n202), .Y(n204));
  or02   g153(.A0(n204), .A1(n200), .Y(n205));
  and02  g154(.A0(n205), .A1(CTS_REG_SCAN_IN), .Y(n206));
  and02  g155(.A0(n65), .A1(RTR), .Y(n207));
  and02  g156(.A0(n207), .A1(n84), .Y(n208));
  and02  g157(.A0(n174), .A1(n68), .Y(n209));
  or02   g158(.A0(n209), .A1(n83), .Y(n210));
  and02   g159(.A0(n210), .A1(n208), .Y(n211));
  or02   g160(.A0(n211), .A1(n206), .Y(U207));
  or02  g161(.A0(n152), .A1(V_IN_2_), .Y(n213));
  and02  g162(.A0(n100), .A1(KEY), .Y(n214));
  and02  g163(.A0(n214), .A1(n57), .Y(n215));
  nor02  g164(.A0(n215), .A1(n213), .Y(n216));
  and02  g165(.A0(n196), .A1(R_BUTTON), .Y(n217));
  nand02 g166(.A0(n217), .A1(n162), .Y(n218));
  or02  g167(.A0(n218), .A1(n167), .Y(n219));
  and02  g168(.A0(n219), .A1(n161), .Y(n220));
  nand02  g169(.A0(n220), .A1(n216), .Y(n221));
  or02  g170(.A0(n220), .A1(VOTO2_REG_SCAN_IN), .Y(n222));
  and02   g171(.A0(n222), .A1(n221), .Y(U241));
  and02  g172(.A0(n194), .A1(G_BUTTON), .Y(n224));
  nor02  g173(.A0(n194), .A1(n163), .Y(n225));
  and02   g174(.A0(n225), .A1(n224), .Y(U242));
  and02  g175(.A0(n152), .A1(V_IN_0_), .Y(n227));
  nor02  g176(.A0(SIGN_REG_3__SCAN_IN), .A1(n77), .Y(n228));
  and02  g177(.A0(n66), .A1(KEY), .Y(n229));
  or02   g178(.A0(n229), .A1(n228), .Y(n230));
  and02  g179(.A0(n230), .A1(STATO_REG_1__SCAN_IN), .Y(n231));
  nand02  g180(.A0(n231), .A1(n227), .Y(n232));
  inv01  g181(.A(n112), .Y(n233));
  nor02 g182(.A0(n233), .A1(n57), .Y(n234));
  and02  g183(.A0(n234), .A1(n72), .Y(n235));
  and02  g184(.A0(n235), .A1(n158), .Y(n236));
  nand02  g185(.A0(n236), .A1(n232), .Y(n237));
  and02  g186(.A0(n236), .A1(VOTO0_REG_SCAN_IN), .Y(n238));
  or02   g187(.A0(n238), .A1(n237), .Y(U243));
endmodule


